from solution import factorial
import pytest


def test_0():
    assert factorial(0) == 1


def test_1():
    assert factorial(1) == 1


def test_2():
    assert factorial(2) == 2


def test_3():
    assert factorial(3) == 6


def test_throws_exception():
    with pytest.raises(ValueError):
        factorial(-1)


def test_throws_exception_2():
    with pytest.raises(ValueError):
        factorial(13)
